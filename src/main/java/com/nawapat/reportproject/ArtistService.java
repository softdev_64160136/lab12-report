/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.reportproject;

import java.util.List;

/**
 *
 * @author asus
 */
public class ArtistService {

    public List<ArtistReport> getTopTenArtistByTotalPrice() {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPricr(10);
    }

    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin, String end) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPricr(begin, end, 10);
    }

}
